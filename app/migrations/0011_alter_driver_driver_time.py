# Generated by Django 4.0.1 on 2022-05-21 17:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_driver_driver_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='driver_time',
            field=models.DateTimeField(blank=True),
        ),
    ]
