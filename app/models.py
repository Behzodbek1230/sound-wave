from django.db import models


class Order(models.Model):
    # name=models.CharField(max_length=50,null=True)
    store_latitude = models.FloatField(max_length=50)
    store_longitude = models.FloatField(max_length=50)
    client_latitude = models.FloatField(max_length=50)
    client_longitude = models.FloatField(max_length=50)
    driver_accept_time = models.DateTimeField(auto_now=True)
    driver_name=models.CharField(max_length=50,blank=True)
    def __str__(self):
        return self.driver_name

class Driver(models.Model):
    name = models.CharField(max_length=50)
    driver_accept_latitude = models.FloatField(max_length=20,null=True)
    driver_accept_longitude = models.FloatField(max_length=50,null=True)
    driver_id=models.ForeignKey(Order,on_delete=models.CASCADE)
    driver_time = models.DateTimeField(blank=True)

    def __str__(self):
        return self.name


class Dist(models.Model):
    driver = models.OneToOneField(Driver, on_delete=models.CASCADE, primary_key=True)
    dist = models.FloatField(max_length=50)
