import json

from django.shortcuts import render
import math
import numpy as np
from numpy.linalg import eig
from .forms import NameForm


class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def wiews(request):
    Z = []
    z1 = []
    z2 = []
    z1y=[]
    z2y=[]
    if request.method == "POST":
        M = int(request.POST['M'])
        N = int(request.POST['N'])
        T = int(request.POST['T'])
        My = int(request.POST['My'])
        lx = int(request.POST['lx'])
        ly = int(request.POST['ly'])
        Cs = 1.4
        Cp1 = 2
        Cp2 = 0.45
        pFol = 1
        pFos = 1.5
        d0 = 0.1

        def fi1(t, x, y):
            return x * math.sin(t * y)

        def fi2(t, x, y):
            return x * math.cos(t * y)

        def fi3(t, x, y):
            return x * t ** 3 * y

        def fi4(t, x, y):
            return x ** 2 + y ** 2 + t ** 2

        def fi5(t, x, y):
            return t * y * x

        def fi6(t, x, y):
            return math.sin(t * y * x)

        def fi7(t, x, y):
            return math.cos(t - x - y)

        def fi8(t, x, y):
            return math.sin(t + y + x)

        p0s = pFos * (1 - d0)
        p0l = pFol * d0
        p0 = p0s + p0l
        myu = p0s * Cs ** 2
        f0 = 1
        K = (p0 * p0s) / (2 * p0l) * (Cp1 ** 2 + Cp2 ** 2 - (8 / 3) * (p0l / p0) * Cs ** 2 -
                                      math.sqrt(
                                          (Cp1 ** 2 - Cp2 ** 2) ** 2 - (64 / 9) * (p0s * p0l / p0 ** 2) * Cs ** 4))
        gamma = 4
        alpha3 = 1 / (2 * p0 ** 2) * (Cp1 ** 2 + Cp2 ** 2 - (8 / 3) * (p0s / p0) * Cs ** 2 +
                                      math.sqrt(
                                          (Cp1 ** 2 - Cp2 ** 2) ** 2 - (64 / 9) * (p0s * p0l / p0 ** 2) * Cs ** 4))
        alpha = p0 * alpha3 + K / p0 ** 2
        t0 = 1.5
        e = K - alpha * p0 * p0s
        gamma = 1
        a = 1 / p0s
        b = 1 / p0
        d = p0s * K / p0
        c = p0l * K / p0 - 2 * myu / 3
        f = alpha * p0 * p0l
        A = [[1, 0, 0, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0, 0], [
            0, 0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 0, 0, 1]]
        B = [[0, 0, 0, 0, a, 0, 0, b], [0, 0, 0, 0, 0, a, 0, 0], [0, 0, 0, 0, 0, 0, 0, b], [0, 0, 0, 0, 0, 0, 0, 0], [
            2 * myu + c, 0, -d, 0, 0, 0, 0, 0], [0, myu, 0, 0, 0, 0, 0, 0], [c, 0, -d, 0, 0, 0, 0, 0],
             [-e, 0, f, 0, 0, 0, 0, 0]]
        C = [[0, 0, 0, 0, 0, a, 0, 0], [0, 0, 0, 0, 0, 0, a, b], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, b], [
            0, c, 0, -d, 0, 0, 0, 0], [myu, 0, 0, 0, 0, 0, 0, 0], [0, 2 * myu + c, 0, -d, 0, 0, 0, 0],
             [0, -e, 0, f, 0, 0, 0, 0]]
        x0 = 24
        y0 = 12
        a = 1
        e = 2.71
        def delta(x, y, x0, y0, a):
            if math.sqrt((x - x0) ** 2 + (y - y0) ** 2) < a:
                return e ** (-a ** 2 / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2))
            else:
                return 0

        def deltax(x, y, x0, y0, a):
            if math.sqrt((x - x0) ** 2 + (y - y0) ** 2) < a:
                return (-2 * a ** 2 * (x - x0) / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2) ** 2) * e ** (
                        -a ** 2 / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2))
            else:
                return 0

        def deltay(x, y, x0, y0, a):
            if math.sqrt((x - x0) ** 2 + (y - y0) ** 2) < a:
                return (-2 * a ** 2 * (y - y0) / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2) ** 2) * e ** (
                        -a ** 2 / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2))
            else:
                return 0

        t0 = 1.5
        f0 = 1

        def f(t):
            return math.math.sin(2 * math.pi * f0 * (t - t0)) * e ** (-(2 * math.pi * f0 * (t - t0)) ** 2) / gamma ** 2

        def F1(t, x, y):
            return f(t) * deltax(x - 2 * t, y, x0, y0, a) * delta(x, y, x0, y0, a)

        def F2(t, x, y):
            return f(t) * deltay(x - 2 * t, y, x0, y0, a) * delta(x, y, x0, y0, a)

        Bw, Bv = eig(B)
        Cw, Cv = eig(C)
        Tt = Cv
        Tinv = np.linalg.inv(Cv)
        Bw.sort()
        Bw = Bw[::-1]
        Bw = np.array([Bw]).T
        Cw.sort()
        Cw = Cw[::-1]
        Cw = np.array([Cw]).T
        def F(t, x, y):
            Array = [0.74 * t * x * math.cos(t * x * y) + x * y * math.cos(t * y) + 0.68 * math.cos(
                x + y + t) + 0.74 * t * y, -x * y * math.sin(y * t) + (
                             0.74 * t * y * math.cos(t * x * y) + 0.68 * math.cos(x + y + t) - 0.74 * math.sin(
                         y + (x - t))), 0.68 * math.cos(y + x + t) + 3 * t ** 2 * x * y,
                     0.68 * math.cos(y + x + t) + 2 * t, (1.57 * t * x + 3.71)
                     * math.sin(t * y) + (x - (2.57 * t ** 3 + 5.15)) * y,
                     x * y * math.cos(t * y * x) + (2.646 * t * x + 2.646) * math.cos(t * y),
                     (-3.71 * t * x - 1.57) * math.sin(t * y) + (math.sin(y + (x - t)) - (2.57 * t ** 3 + 5.15) * y),
                     (-1.76 * t * x + 1.765) * math.sin(t * y) + math.cos(y + x + t) + (0.33 * t ** 3 + 0.67) * y]
            s = np.array([Array]).T
            return s

        def F11(t, x, y):
            s = np.dot(Tinv, F(t, x, y))
            return s
        # print("T",Tt)
        B1 = Tinv * B * Tt
        C1 = Tinv * C * Tt
        x = []
        y = []
        t = []
        E = np.identity(8)
        w = 2
        B1 = B1 + w * E
        h = float(lx / M)
        hy = float(ly / My)
        tao = float(T / N)

        x = np.arange(M + 1, dtype='f').reshape(M + 1)
        y = np.arange(My + 1, dtype='f').reshape(My + 1)
        t = np.arange(N + 1, dtype='f').reshape(N + 1)

        for i in range(0, M + 1):
            x[i] = 21 + i * h
        for n in range(0, N + 1):
            t[n] = (tao * n)
        for j in range(0, My + 1):
            y[j] = (7 + j * hy)
        arr_m = np.arange(6).reshape(2, 3)

        u1 = np.zeros((M + 1, My + 1))
        u2 = np.zeros((M + 1, My + 1))
        u3 = np.zeros((M + 1, My + 1))
        u4 = np.zeros((M + 1, My + 1))
        u5 = np.zeros((M + 1, My + 1))
        u6 = np.zeros((M + 1, My + 1))
        u7 = np.zeros((M + 1, My + 1))
        u8 = np.zeros((M + 1, My + 1))
        w1 = np.zeros((M + 1, My + 1))
        w2 = np.zeros((M + 1, My + 1))
        w3 = np.zeros((M + 1, My + 1))
        w4 = np.zeros((M + 1, My + 1))
        w5 = np.zeros((M + 1, My + 1))
        w6 = np.zeros((M + 1, My + 1))
        w7 = np.zeros((M + 1, My + 1))
        w8 = np.zeros((M + 1, My + 1))
        v1 = np.zeros((M + 1, My + 1))
        v2 = np.zeros((M + 1, My + 1))
        v3 = np.zeros((M + 1, My + 1))
        v4 = np.zeros((M + 1, My + 1))
        v5 = np.zeros((M + 1, My + 1))
        v6 = np.zeros((M + 1, My + 1))
        v7 = np.zeros((M + 1, My + 1))
        v8 = np.zeros((M + 1, My + 1))

        S = []
        Vi = np.arange(M + 1, dtype=object).reshape(M + 1)
        aa = np.arange((M + 1) * (My + 1)).reshape(M + 1, My + 1)
        bb = np.arange((M + 1) * (My + 1)).reshape(M + 1, My + 1)
        def E():
            for i in range(0, M + 1):
                for j in range(0, My + 1):
                    u1[i][j] = fi1(0, x[i], y[j])
                    u2[i][j] = fi2(0, x[i], y[j])
                    u3[i][j] = fi3(0, x[i], y[j])
                    u4[i][j] = fi4(0, x[i], y[j])
                    u5[i][j] = fi5(0, x[i], y[j])
                    u6[i][j] = fi6(0, x[i], y[j])
                    u7[i][j] = fi7(0, x[i], y[j])
                    u8[i][j] = fi8(0, x[i], y[j])

            Array = [u1, u2, u3, u4, u5, u6, u7, u8]
            S.append(np.array(Array))
            for n in range(0, N):
                for i in range(0, M + 1):
                    w3[i][0] = fi3(t[n + 1], x[i], y[0])
                    w2[i][0] = fi2(t[n + 1], x[i], y[0])
                    w1[i][0] = fi1(t[n + 1], x[i], y[0])
                    w4[i][0] = fi4(t[n + 1], x[i], y[0])
                    w5[i][0] = fi5(t[n + 1], x[i], y[0])
                    for j in range(1, My + 1):
                        w1[i][j] = u1[i][j] - (tao / hy) * C1[0][0] * (u1[i][j] - u1[i][j - 1])
                        w2[i][j] = u2[i][j] - (tao / hy) * C1[1][1] * (u2[i][j] - u2[i][j - 1])
                        w3[i][j] = u3[i][j] - (tao / hy) * C1[2][2] * (u3[i][j] - u3[i][j - 1])
                        w4[i][j] = u4[i][j]
                        w5[i][j] = u5[i][j]

                    for j in range(0, My):
                        w6[i][j] = u6[i][j] - (tao / hy) * C1[5][5] * (u6[i][j + 1] - u6[i][j])
                        w7[i][j] = u7[i][j] - (tao / hy) * C1[6][6] * (u7[i][j + 1] - u7[i][j])
                        w8[i][j] = u8[i][j] - (tao / hy) * C1[7][7] * (u8[i][j + 1] - u8[i][j])

                    w6[i][My] = fi6(t[n + 1], x[i], y[My])
                    w7[i][My] = fi7(t[n + 1], x[i], y[My])
                    w8[i][My] = fi8(t[n + 1], x[i], y[My])
                    w4[i][0] = u4[i][0]
                    w5[i][0] = u5[i][0]
                for j in range(0, My + 1):
                    for i in range(1, M):
                        v1[0][j] = w1[0][j]
                        v2[0][j] = w2[0][j]
                        v3[0][j] = w3[0][j]
                        v4[0][j] = w4[0][j]
                        v5[0][j] = w5[0][j]
                        v6[0][j] = w6[0][j]
                        v7[0][j] = w7[0][j]
                        v8[0][j] = w8[0][j]
                        v1[M][j] = w1[M - 1][j]
                        v2[M][j] = w2[M - 1][j]
                        v3[M][j] = w3[M - 1][j]
                        v4[M][j] = w4[M - 1][j]
                        v5[M][j] = w5[M - 1][j]
                        v6[M][j] = w6[M - 1][j]
                        v7[M][j] = w7[M - 1][j]
                        v8[M][j] = w8[M - 1][j]
                        v1[i][j] = w1[i][j] - (tao / (1 * h)) * (
                                B1[0][0] * (w1[i][j] - w1[i - 1][j]) + B1[0][1] * (
                                w2[i][j] - w2[i - 1][j]) + B1[0][2] * (w3[i][j] - w3[i - 1][j]) +
                                B1[0][3] * (
                                        w4[i][j] - w4[i - 1][j]) + B1[0][4] * (
                                        w5[i][j] - w5[i - 1][j]) + B1[0][5] * (
                                        w6[i][j] - w6[i - 1][j]) + B1[0][6] * (
                                        w7[i][j] - w7[i - 1][j]) + B1[0][7] * (
                                        w8[i][j] - w8[i - 1][j]))
                        v2[i][j] = w2[i][j] - (tao / (1 * h)) * (
                                B1[1][0] * (w1[i][j] - w1[i - 1][j]) + B1[1][1] * (
                                w2[i][j] - w2[i - 1][j]) + B1[1][2] * (w3[i][j] - w3[i - 1][j]) +
                                B1[1][3] * (
                                        w4[i][j] - w4[i - 1][j]) + B1[1][4] * (
                                        w5[i][j] - w5[i - 1][j]) + B1[1][5] * (
                                        w6[i][j] - w6[i - 1][j]) + B1[1][6] * (
                                        w7[i][j] - w7[i - 1][j]) + B1[1][7] * (
                                        w8[i][j] - w8[i - 1][j]))
                        v3[i][j] = w3[i][j] - (tao / (1 * h)) * (
                                B1[2][0] * (w1[i][j] - w1[i - 1][j]) + B1[2][1] * (
                                w2[i][j] - w2[i - 1][j]) + B1[2][2] * (w3[i][j] - w3[i - 1][j]) +
                                B1[2][3] * (
                                        w4[i][j] - w4[i - 1][j]) + B1[2][4] * (
                                        w5[i][j] - w5[i - 1][j]) + B1[2][5] * (
                                        w6[i][j] - w6[i - 1][j]) + B1[2][6] * (
                                        w7[i][j] - w7[i - 1][j]) + B1[2][7] * (
                                        w8[i][j] - w8[i - 1][j]))
                        v4[i][j] = w4[i][j] - (tao / (1 * h)) * (
                                B1[3][0] * (w1[i][j] - w1[i - 1][j]) + B1[3][1] * (
                                w2[i][j] - w2[i - 1][j]) + B1[3][2] * (w3[i][j] - w3[i - 1][j]) +
                                B1[3][3] * (
                                        w4[i][j] - w4[i - 1][j]) + B1[3][4] * (
                                        w5[i][j] - w5[i - 1][j]) + B1[3][5] * (
                                        w6[i][j] - w6[i - 1][j]) + B1[3][6] * (
                                        w7[i][j] - w7[i - 1][j]) + B1[3][7] * (
                                        w8[i][j] - w8[i - 1][j]))
                        v5[i][j] = w5[i][j] - (tao / (1 * h)) * (
                                B1[4][0] * (w1[i][j] - w1[i - 1][j]) + B1[4][1] * (
                                w2[i][j] - w2[i - 1][j]) + B1[4][2] * (w3[i][j] - w3[i - 1][j]) +
                                B1[4][3] * (
                                        w4[i][j] - w4[i - 1][j]) + B1[4][4] * (
                                        w5[i][j] - w5[i - 1][j]) + B1[4][5] * (
                                        w6[i][j] - w6[i - 1][j]) + B1[4][6] * (
                                        w7[i][j] - w7[i - 1][j]) + B1[4][7] * (
                                        w8[i][j] - w8[i - 1][j]))
                        v6[i][j] = w6[i][j] - (tao / (1 * h)) * (
                                B1[5][0] * (w1[i][j] - w1[i - 1][j]) + B1[5][1] * (
                                w2[i][j] - w2[i - 1][j]) + B1[5][2] * (w3[i][j] - w3[i - 1][j]) +
                                B1[5][3] * (
                                        w4[i][j] - w4[i - 1][j]) + B1[5][4] * (
                                        w5[i][j] - w5[i - 1][j]) + B1[5][5] * (
                                        w6[i][j] - w6[i - 1][j]) + B1[5][6] * (
                                        w7[i][j] - w7[i - 1][j]) + B1[5][7] * (
                                        w8[i][j] - w8[i - 1][j]))
                        v7[i][j] = w7[i][j] - (tao / (1 * h)) * (
                                B1[6][0] * (w1[i][j] - w1[i - 1][j]) + B1[6][1] * (
                                w2[i][j] - w2[i - 1][j]) + B1[6][2] * (w3[i][j] - w3[i - 1][j]) +
                                B1[6][3] * (
                                        w4[i][j] - w4[i - 1][j]) + B1[6][4] * (
                                        w5[i][j] - w5[i - 1][j]) + B1[6][5] * (
                                        w6[i][j] - w6[i - 1][j]) + B1[6][6] * (
                                        w7[i][j] - w7[i - 1][j]) + B1[6][7] * (
                                        w8[i][j] - w8[i - 1][j]))
                        v8[i][j] = w8[i][j] - (tao / (1 * h)) * (
                                B1[7][0] * (w1[i][j] - w1[i - 1][j]) + B1[7][1] * (
                                w2[i][j] - w2[i - 1][j]) + B1[7][2] * (w3[i][j] - w3[i - 1][j]) +
                                B1[7][3] * (
                                        w4[i][j] - w4[i - 1][j]) + B1[7][4] * (
                                        w5[i][j] - w5[i - 1][j]) + B1[7][5] * (
                                        w6[i][j] - w6[i - 1][j]) + B1[7][6] * (
                                        w7[i][j] - w7[i - 1][j]) + B1[7][7] * (
                                        w8[i][j] - w8[i - 1][j]))
                for j in range(0, My + 1):
                    for i in range(0, M + 1):
                        u1[i][j] = v1[i][j] + tao * F11(t[n], x[i], y[j])[0][0]
                        u2[i][j] = v2[i][j] + tao * F11(t[n], x[i], y[j])[1][0]
                        u3[i][j] = v3[i][j] + tao * F11(t[n], x[i], y[j])[2][0]
                        u6[i][j] = v6[i][j] + tao * F11(t[n], x[i], y[j])[5][0]
                        u7[i][j] = v7[i][j] + tao * F11(t[n], x[i], y[j])[6][0]
                        u8[i][j] = v8[i][j] + tao * F11(t[n], x[i], y[j])[7][0]
                Array = [u1, u2, u3, u4, u5, u6, u7, u8]
                S.append(np.array(Array))
            return S

        def E1():
            for n in range(1, N):
                for i in range(0, M):
                    for j in range(0, My):
                        Array = [E()[n][j][i][0][0], E()[n][j][i][1][0], E()[n][j][i][2][0], E()[n][j][i][3][0], E()[
                            n][j][i][4][0], E()[n][j][i][5][0], E()[n][j][i][6][0], E()[n][j][i][7][0]]
                    Vi[i] = np.dot(Cv, np.array(Array))
                S[n] = np.array(Vi)

            return S

        def E2(E1, k1):
            for i in range(0, M):
                for j in range(0, My):
                    bb[i][j] = math.sqrt(
                        (E1()[k1][0][i][j]) * (E1()[k1][0][i][j]) + (E1()[k1][1][i][j]) * (
                            E1()[k1][1][i][j]))
                    aa[i][j] = math.sqrt(
                        (E1()[k1][2][i][j]) * (E1()[k1][2][i][j]) + (E1()[k1][3][i][j]) * (
                            E1()[k1][3][i][j]))
            return [aa, bb]

        list = E()
        Z.append(json.dumps(list, cls=NumpyEncoder))
        for n in range(N+1):
            zx=[]
            for i in range(M+1):
                zy=[]
                for j in range(My+1):
                    zy.append(fi1(t[n],x[i],y[j]))
                zx.append(zy)
            z1.append(zx)
        for n in range(N + 1):
            zxy = []
            for j in range(My + 1):
                zyy = []
                for i in range(M + 1):
                    zyy.append(fi1(t[n], x[i], y[j]))
                zxy.append(zy)
            z1y.append(zxy)

        for i in range(M + 1):
            for j in range(My + 1):
                z2.append(u1)

    else:
        return render(request, 'base.html')
    context = {}
    values = {
        "M": M, "N": N, "T": T, "My": My, "lx": lx, "ly": ly

    }
    if (Z):
        context = {
            "M": M, "N": N, "T": T, "My": My, "lx": lx, "ly": ly,
            "data": Z[0],
            "z1": z1,
            "z2": z1y,
            "tao":tao,
            "h":h,
            "hy":hy,
        }
    else:


        context = {
            "M": M, "N": N, "T": T, "My": My, "lx": lx, "ly": ly,
            "data": Z,
            "z1": z1,
            "z2": z1y
        }

    return render(request, 'base.html', context)
