# Generated by Django 4.0.1 on 2022-05-21 12:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_alter_order_driver_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='driver_name',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
